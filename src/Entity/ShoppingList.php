<?php

namespace App\Entity;

use App\Repository\ShoppingListRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ShoppingListRepository::class)]
class ShoppingList
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'shoppingLists')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\OneToMany(mappedBy: 'shoppingList', targetEntity: ShoppingListProduct::class, orphanRemoval: true)]
    private Collection $shoppingListProducts;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $created = null;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return Collection<int, ShoppingListProduct>
     */
    public function getShoppingListProducts(): Collection
    {
        return $this->shoppingListProducts;
    }

    public function addShoppingListProduct(ShoppingListProduct $shoppingListProduct): self
    {
        if (!$this->shoppingListProducts->contains($shoppingListProduct)) {
            $this->shoppingListProducts->add($shoppingListProduct);
            $shoppingListProduct->setShoppingList($this);
        }

        return $this;
    }

    public function removeShoppingListProduct(ShoppingListProduct $shoppingListProduct): self
    {
        if ($this->shoppingListProducts->removeElement($shoppingListProduct)) {
            // set the owning side to null (unless already changed)
            if ($shoppingListProduct->getShoppingList() === $this) {
                $shoppingListProduct->setShoppingList(null);
            }
        }

        return $this;
    }
}
