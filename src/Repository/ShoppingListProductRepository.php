<?php

namespace App\Repository;

use App\Entity\ShoppingListProduct;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ShoppingListProduct>
 *
 * @method ShoppingListProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShoppingListProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShoppingListProduct[]    findAll()
 * @method ShoppingListProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShoppingListProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShoppingListProduct::class);
    }

    public function save(ShoppingListProduct $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ShoppingListProduct $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Check if ShoppingListProduct is shared to the given user
     *
     * @param ShoppingListProduct $shoppingListProduct
     * @param User $user
     * @return bool
     */
    public function isSharedToUser(ShoppingListProduct $shoppingListProduct, User $user): bool
    {
        $sharedTo = $shoppingListProduct->getSharedTo();

        $expr = new Comparison('id', '=', $user->getId());
        $criteria = new Criteria();
        $criteria->where($expr);

        /** @var ArrayCollection $isShared */
        $isShared = $sharedTo->matching($criteria);

        return $isShared->count() > 0;
    }

    /**
     * Return an array of ShoppingListProducts grouped by ShoppingList which are shared to the given user
     * @param User $user
     * @return array
     */
    public function getShoppingListProductsSharedInToUserGroupByShoppingList(User $user): array
    {
        $shoppingListProductsSharedIn = $this->getShoppingListProductsSharedInToUser($user);
        $arrayResult = [];

        /** @var ShoppingListProduct $shoppingListProductSharedIn */
        foreach ($shoppingListProductsSharedIn as $shoppingListProductSharedIn)
            $arrayResult[$shoppingListProductSharedIn->getShoppingList()->getId()][] = $shoppingListProductSharedIn;

        return $arrayResult;
    }

    /**
     * Return an array of ShoppingListProducts grouped by ShoppingList which are shared by the given user
     * @param User $user
     * @return array
     */
    public function getShoppingListProductsSharedOutForUserGroupByShoppingList(User $user): array
    {
        $shoppingListProductsSharedOut = $this->getShoppingListProductsSharedOutToUser($user);
        $arrayResult = [];

        /** @var ShoppingListProduct $shoppingListProductSharedIn */
        foreach ($shoppingListProductsSharedOut as $shoppingListProductSharedOut) {
            $arrayResult[$shoppingListProductSharedOut->getShoppingList()->getId()][] = $shoppingListProductSharedOut;
        }

        return $arrayResult;
    }

    /**
     * Return an array of ShoppingListProducts which are shared to the given user
     * @param User $user
     * @return float|int|mixed|string
     */
    public function getShoppingListProductsSharedInToUser(User $user): mixed
    {
        $qb = $this->createQueryBuilder('slp')
            ->innerJoin('slp.sharedTo', 'sharedTo')
            ->where('sharedTo.id = :userId')
            ->setParameter('userId', $user->getId());

        return $qb->getQuery()->getResult();
    }

    /**
     * Return an array of ShoppingListProducts which are shared by the given user
     * @param User $user
     * @return float|int|mixed|string
     */
    public function getShoppingListProductsSharedOutToUser(User $user): mixed
    {
        $shoppingListProductsSharedOut = [];

        $shoppingListProductsShared = $this->createQueryBuilder('slp')
            ->innerJoin('slp.sharedTo', 'sharedTo')
            ->where('sharedTo.id IS NOT NULL')
            ->getQuery()
            ->getResult()
        ;

        /** @var ShoppingListProduct $shoppingListProductShared */
        foreach ($shoppingListProductsShared as $shoppingListProductShared){
            if($shoppingListProductShared->getShoppingList()->getUser() === $user)
                $shoppingListProductsSharedOut[] = $shoppingListProductShared;
        }

        return $shoppingListProductsSharedOut;
    }
}
