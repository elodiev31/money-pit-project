<?php

namespace App\TwigExtension;

use App\Entity\ShoppingList;
use App\Entity\ShoppingListProduct;
use App\Entity\User;
use App\Service\ShareService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ShareExtension extends AbstractExtension
{
    public function __construct(private readonly ShareService $shareService)
    {}

    /**
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
        return array(
            new TwigFunction('getUsersWhomShoppingListHasBeenShared', array($this, 'getUsersWhomShoppingListHasBeenShared')),
        );
    }

    /**
     * Return an array of users to whom the given shopping list has been shared
     * @param ShoppingList $shoppingList
     * @return array
     */
    public function getUsersWhomShoppingListHasBeenShared(ShoppingList $shoppingList)
    {
        return $this->shareService->getUsersWhomShoppingListHasBeenShared($shoppingList);
    }
}