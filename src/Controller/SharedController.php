<?php

namespace App\Controller;

use App\Entity\ShoppingListProduct;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route(path: '/shared')]
class SharedController extends AbstractController
{
    private ObjectManager $manager;

    /**
     * @param ManagerRegistry $doctrine
     */
    public function __construct(private readonly ManagerRegistry $doctrine, private readonly TranslatorInterface $translator)
    {
        $this->manager = $this->doctrine->getManager();
    }


    /**
     * @return Response
     */
    #[Route(path: '', name: 'shared_crud_page')]
    public function sharedCrudAction(): Response
    {
        $user = $this->manager->getRepository(User::class)->find($this->getUser()->getId());
        $sharedIn = [];
        $sharedOut = [];

        if(empty($user)){
           $this->addFlash('danger', $this->translator->trans('error.no_user_found'));

           return $this->redirectToRoute('homepage');
        }

        $shoppingListProductsSharedIn = $this->manager->getRepository(ShoppingListProduct::class)
            ->getShoppingListProductsSharedInToUserGroupByShoppingList($user);

        foreach ($shoppingListProductsSharedIn as $shoppingList) {
            /** @var ShoppingListProduct $shoppingListProduct */
            foreach ($shoppingList as $shoppingListProduct){
                $shoppingListName = $shoppingListProduct->getShoppingList()->getName();
                $sharedIn[$shoppingListName]['user'] = $shoppingListProduct->getShoppingList()->getUser()->getUsername();

                if (empty($sharedIn[$shoppingListName]['products']))
                    $sharedIn[$shoppingListName]['products'] = $shoppingListProduct->getProduct();
                else
                    $sharedIn[$shoppingListName]['products'] .= ', ' . $shoppingListProduct->getProduct();

                if (!empty($shoppingListProduct->getQuantity()))
                    $sharedIn[$shoppingListName]['products'] .= ' (' . $shoppingListProduct->getQuantity() . ')';
            }
        }

        $shoppingListProductsSharedOut = $this->manager->getRepository(ShoppingListProduct::class)
            ->getShoppingListProductsSharedOutForUserGroupByShoppingList($user);

        foreach ($shoppingListProductsSharedOut as $shoppingList) {
                /** @var ShoppingListProduct $shoppingListProduct */
                foreach ($shoppingList as $shoppingListProduct){
                    $shoppingListName = $shoppingListProduct->getShoppingList()->getName();
                    $sharedOut[$shoppingListName]['user'] = $shoppingListProduct->getShoppingList()->getUser()->getUsername();
                    $sharedOut[$shoppingListName]['shoppingListId'] = $shoppingListProduct->getShoppingList()->getId();

                    if (empty($sharedIn[$shoppingListName]['products']))
                        $sharedOut[$shoppingListName]['products'] = $shoppingListProduct->getProduct();
                    else
                        $sharedOut[$shoppingListName]['products'] .= ', ' . $shoppingListProduct->getProduct();

                    if (!empty($shoppingListProduct->getQuantity()))
                        $sharedOut[$shoppingListName]['products'] .= ' (' . $shoppingListProduct->getQuantity() . ')';
                }
            }

        return $this->render('shared/crud.html.twig', [
            'shoppingListProductsSharedIn' => $sharedIn,
            'shoppingListProductsSharedOut' => $sharedOut
        ]);
    }
}