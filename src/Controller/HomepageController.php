<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * Default homepage when the user is no logged
     *
     * @return Response
     */
    #[Route('/', name: 'homepage')]
    public function homepage(): Response
    {
        if (!empty($this->getUser()))
            return $this->redirectToRoute('shopping_list_crud');

        return $this->render('main/homepage.html.twig');
    }


}