<?php

namespace App\Controller;

use App\Entity\ShoppingList;
use App\Entity\ShoppingListProduct;
use App\Form\ShoppingListProduct\AddUpdateType;
use App\Form\ShoppingListProduct\ShareType;
use App\Service\ShareService;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
#[Route('/shopping-list/{shoppingListId}/product')]
class ShoppingListProductController extends AbstractController
{
    private ObjectManager $manager;

    /**
     * @param ManagerRegistry $doctrine
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     * @param ShareService $shareService
     */
    public function __construct(
        private readonly ManagerRegistry     $doctrine,
        private readonly TranslatorInterface $translator,
        private readonly LoggerInterface $logger,
        private readonly ShareService $shareService
    ) {
        $this->manager = $this->doctrine->getManager();
    }


    /**
     * @param Request $request
     * @param int $shoppingListId
     * @return RedirectResponse|Response
     */
    #[Route('/add', name: 'shopping_list_product_add')]
    public function addAction(Request $request, int $shoppingListId): RedirectResponse|Response
    {
        $shoppingList = $this->manager->getRepository(ShoppingList::class)->find($shoppingListId);

        if (gettype($shoppingList) === 'string') {
            $this->addFlash('danger', $shoppingList);

            return $this->redirectToRoute('shopping_list_crud');
        }

        $shoppingListProduct = new ShoppingListProduct();
        $shoppingListProduct->setShoppingList($shoppingList);

        $form = $this->createForm(AddUpdateType::class, $shoppingListProduct);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            try {
                $this->manager->persist($shoppingListProduct);
                $this->manager->flush();

                $this->addFlash('success', $this->translator->trans('msg.success.add', ['%listName' => $shoppingList->getName()], 'shopping_list_product'));

                return $this->redirectToRoute('shopping_list_show', ['id' => $shoppingList->getId()]);
            }
            catch (Exception $exception){
                $this->logger->error($exception->getMessage());
            }
        }

        return $this->render('ShoppingListProduct/form/add-update.html.twig', [
            'form' => $form->createView(),
            'shoppingListProduct' => $shoppingListProduct
        ]);
    }

    /**
     * @param Request $request
     * @param int $shoppingListId
     * @param int $id
     * @return RedirectResponse|Response
     */
    #[Route('/{id}/update', name: 'shopping_list_product_update')]
    public function updateAction(Request $request, int $shoppingListId, int $id): RedirectResponse|Response
    {
        $shoppingList = $this->manager->getRepository(ShoppingList::class)->find($shoppingListId);
        $shoppingListProduct = $this->manager->getRepository(ShoppingListProduct::class)->find($id);

        if (empty($shoppingList) || empty($shoppingListProduct)) {
            $this->addFlash('danger', $this->translator->trans('error.error'));

            return $this->redirectToRoute('shopping_list_crud');
        }

        $form = $this->createForm(AddUpdateType::class, $shoppingListProduct);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            try {
                $this->manager->persist($shoppingListProduct);
                $this->manager->flush();

                $this->addFlash('success', $this->translator->trans('msg.success.update', ['%listName' => $shoppingList->getName()], 'shopping_list_product'));

                return $this->redirectToRoute('shopping_list_show', ['id' => $shoppingList->getId()]);
            }
            catch (Exception $exception){
                $this->logger->error($exception->getMessage());
            }
        }

        return $this->render('ShoppingListProduct/form/add-update.html.twig', [
            'form' => $form->createView(),
            'shoppingListProduct' => $shoppingListProduct
        ]);
    }

    /**
     * @param int $shoppingListId
     * @param int $id
     * @return RedirectResponse
     */
    #[Route(path: '/delete/{id}', name: 'shopping_list_product_delete')]
    public function deleteAction(int $shoppingListId, int $id): RedirectResponse
    {
        $shoppingList = $this->manager->getRepository(ShoppingList::class)->find($shoppingListId);
        $shoppingListProduct = $this->manager->getRepository(ShoppingListProduct::class)->find($id);

        if (empty($shoppingList) || empty($shoppingListProduct)) {
            $this->addFlash('danger', $this->translator->trans('error.error'));

            return $this->redirectToRoute('shopping_list_crud');
        }

        try {
            $this->manager->remove($shoppingListProduct);
            $this->manager->flush();

            $this->addFlash('success', $this->translator->trans('msg.success.delete', [
                '%product' => $shoppingListProduct->getProduct(),
                '%listName' => $shoppingListProduct->getShoppingList()->getName()
            ], 'shopping_list_product'));

        } catch (Exception $exception) {
                    $this->logger->error($exception->getMessage());
                }

        return $this->redirectToRoute('shopping_list_show', ['id' => $shoppingList->getId()]);
    }

    /**
     * Form + action to share a shoppingListProduct
     *
     * @param Request $request
     * @param int $shoppingListId
     * @param int $id
     * @return RedirectResponse|Response
     */
    #[Route(path: '/{id}/share', name: 'shopping_list_product_share')]
    public function shareAction(Request $request, int $shoppingListId, int $id): RedirectResponse|Response
    {
        // Check if shoppingList and shoppingListProduct exist
        $shoppingListProductRepository = $this->manager->getRepository(ShoppingListProduct::class);
        $shoppingList = $this->manager->getRepository(ShoppingList::class)->find($shoppingListId);
        $shoppingListProduct = $shoppingListProductRepository->find($id);

        if (empty($shoppingList)) {
            $this->addFlash('danger', $this->translator->trans('msg.error.not_found', [], 'shopping_list'));
            return $this->redirectToRoute('shopping_list_crud');
        }
        elseif (empty($shoppingListProduct)) {
            $this->addFlash('danger', $this->translator->trans('msg.error.not_found', [], 'shopping_list_product'));
            return $this->redirectToRoute('shopping_list_crud');
        }

        $form = $this->createForm(ShareType::class, null, [
                'currentUserId' => $this->getUser()->getId(),
                'users' => $shoppingListProduct->getSharedTo()
        ]);

        if(isset($request->request->all()['share']) && !empty($data = $request->request->all()['share'])) {

            try {
                $this->shareService->shareShoppingListProductToUsers($shoppingListProduct, $data['users'] ?? []);

                $this->addFlash('success', $this->translator->trans('msg.success.share', [
                    '%product' => $shoppingListProduct->getProduct()
                ], 'shopping_list_product'));

                return $this->redirectToRoute('shopping_list_show', ['id' => $shoppingListId]);
            }
            catch (Exception $exception){
                $this->logger->error($exception->getMessage());
            }

        }

        return $this->render('ShoppingListProduct\form\share.html.twig', [
            'shoppingListProduct' => $shoppingListProduct,
            'form' => $form->createView()
        ]);
    }
}