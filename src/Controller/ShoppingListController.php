<?php

namespace App\Controller;

use App\Entity\ShoppingList;
use App\Entity\User;
use App\Form\ShoppingList\CreateEditType;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/shopping-list')]
class ShoppingListController extends AbstractController
{
    private ObjectManager $manager;

    /**
     * @param ManagerRegistry $doctrine
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     */
    public function __construct(
        private readonly ManagerRegistry     $doctrine,
        private readonly TranslatorInterface $translator,
        private readonly LoggerInterface $logger,
    ) {
        $this->manager = $this->doctrine->getManager();
    }

    /**
     * @return Response
     */
    #[Route(path: '/', name: 'shopping_list_crud')]
    public function CrudAction(): Response
    {
        if (empty($this->getUser())){
            $this->addFlash('danger', $this->translator->trans('error.no_user', [], 'messages'));
            return $this->redirectToRoute('homepage');
        }

        $shoppingLists = $this->manager->getRepository(ShoppingList::class)->findBy(['user' => $this->getUser()->getId()]);

        return $this->render('ShoppingList/crud.html.twig', [
            'shoppingLists' => $shoppingLists
        ]);
    }

    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    #[Route(path: '/{id}', name: 'shopping_list_show')]
    public function showAction($id): RedirectResponse|Response
    {
        $shoppingList = $this->manager->getRepository(ShoppingList::class)->find($id);

        if (empty($shoppingList)) {
            $this->addFlash('danger', $this->translator->trans('msg.error.not_found', [], 'shopping_list'));

            return $this->redirectToRoute('shopping_list_crud');
        }

        return $this->render('ShoppingList/detail.html.twig', [
            'shoppingList' => $shoppingList
        ]);
    }

    /**
     * @param Request $request
     * @param string $modalType
     * @return JsonResponse|\JsonException
     */
    #[Route(path: '/modal/{modalType}', name: 'shopping_list_modal')]
    public function openShoppingListModalAction(Request $request, string $modalType): JsonResponse|\JsonException
    {
        $html = null;
        $shoppingList = $this->manager->getRepository(ShoppingList::class)->find($request->get('id'));

        if (empty($shoppingList)) {
            $this->addFlash('danger', $this->translator->trans('msg.error.not_found', [], 'shopping_list'));

            return new JsonResponse([
                'success' => false,
            ]);
        }

        if ($modalType === 'delete') {

            $html = $this->renderView('ShoppingList/modal/delete.html.twig',[
                'modalId' => 'shopping-list-modal-delete',
                'shoppingList' => $shoppingList
            ]);

        }

        if (!empty($html))
            return new JsonResponse($html);
        else {
            $this->logger->error($this->translator->trans('error.modal', [], 'messages'));
            return new \JsonException(
                $this->translator->trans('error.error'),
                500
            );
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     */
    #[Route(path: '/create-edit/create', name: 'shopping_list_create')]
    public function create(Request $request): RedirectResponse|Response
    {
        $shoppingList = new ShoppingList();

        $form = $this->createForm(CreateEditType::class, $shoppingList);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            try {
                $shoppingList->setCreated(new \DateTime());
                $shoppingList->setUser($this->getUser());

                $this->manager->persist($shoppingList);
                $this->manager->flush();

                $this->addFlash('success', $this->translator->trans('msg.success.add', [], 'shopping_list'));

                return $this->redirectToRoute('shopping_list_show', ['id' => $shoppingList->getId()]);
            }
            catch (\Exception $exception){
                $this->logger->error($exception->getMessage());
            }
        }

        return $this->render('ShoppingList/form/create-edit.html.twig', [
            'form' => $form->createView(),
            'shoppingList' => $shoppingList
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return RedirectResponse|Response
     */
    #[Route(path: '/{id}/edit', name: 'shopping_list_edit')]
    public function editAction(Request $request, int $id): RedirectResponse|Response
    {
        $shoppingList = $this->manager->getRepository(ShoppingList::class)->find($id);

        if (empty($shoppingList)) {
            $this->addFlash('danger', $this->translator->trans('msg.error.not_found', [], 'shopping_list'));

            return $this->redirectToRoute('shopping_list_crud');
        }

        $form = $this->createForm(CreateEditType::class, $shoppingList);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            try {

                $this->manager->persist($shoppingList);
                $this->manager->flush();

                $this->addFlash('success', $this->translator->trans('msg.success.update', [], 'shopping_list'));

                return $this->redirectToRoute('shopping_list_crud');
            }
            catch (\Exception $exception){
                $this->logger->error($exception->getMessage());
            }
        }

        return $this->render('ShoppingList/form/create-edit.html.twig', [
            'form' => $form->createView(),
            'shoppingList' => $shoppingList
        ]);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    #[Route(path: '/{id}/delete', name: 'shopping_list_delete')]
    public function deleteAction(int $id): JsonResponse
    {
        $shoppingList = $this->manager->getRepository(ShoppingList::class)->find($id);

        if (!empty($shoppingList)) {
            try {
                $this->manager->remove($shoppingList);
                $this->manager->flush();

                $this->addFlash('success', $this->translator->trans('msg.success.delete', [], 'shopping_list'));

                return new JsonResponse(['success' => true]);
            }
            catch (\Exception $exception){
                $this->logger->error($exception->getMessage());
            }
        }

        $this->addFlash('danger', $this->translator->trans('msg.error.not_found', [], 'shopping_list'));

        return new JsonResponse(['success' => false]);
    }
}