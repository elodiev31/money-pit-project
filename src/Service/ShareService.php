<?php

namespace App\Service;

use App\Entity\ShoppingList;
use App\Entity\ShoppingListProduct;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;

class ShareService
{
    private ObjectManager $manager;

    /**
     * @param ManagerRegistry $doctrine
     */
    public function __construct(
        private readonly ManagerRegistry     $doctrine,
    ) {
        $this->manager = $this->doctrine->getManager();
    }

    /**
     * Check if the ShoppingListProduct is shared to the given user
     * @param ShoppingListProduct $shoppingListProduct
     * @param User $user
     * @return bool
     */
    public function isShoppingListProductSharedToUser(ShoppingListProduct $shoppingListProduct, User $user): bool
    {
        return $this->manager->getRepository(ShoppingListProduct::class)->isSharedToUser($shoppingListProduct, $user);
    }

    /**
     * Return an array of users to whom the given shopping list has been shared
     * @param ShoppingList $shoppingList
     * @return array
     */
    public function getUsersWhomShoppingListHasBeenShared(ShoppingList $shoppingList): array
    {
        return $this->manager->getRepository(User::class)->getUsersWhomShoppingListHasBeenShared($shoppingList);
    }

    /**
     * Share ShoppingListProduct to given users
     * @param ShoppingListProduct $shoppingListProduct
     * @param array $userIds
     * @return void
     */
    public function shareShoppingListProductToUsers(ShoppingListProduct $shoppingListProduct, array $userIds = []): void
    {
        $userRepository = $this->manager->getRepository(User::class);

        if (!empty($userIds)) {
            foreach ($userIds as $userId) {

                /** @var User $user */
                $user = $userRepository->find($userId);

                if (!empty($user->getId())) {

                    if (!$this->isShoppingListProductSharedToUser($shoppingListProduct, $user)) {
                        $shoppingListProduct->addSharedTo($user);
                        $this->manager->persist($shoppingListProduct);
                    }
                }

            }

            $this->manager->flush();
        }

        $sharedToUsers = $shoppingListProduct->getSharedTo()->toArray();

        foreach ($sharedToUsers as $sharedToUser) {
            if (!in_array($sharedToUser->getId(), $userIds)) {
                $shoppingListProduct->removeSharedTo($sharedToUser);
                $sharedToUser->removeShoppingListProduct($shoppingListProduct);

                $this->manager->persist($shoppingListProduct);
                $this->manager->persist($sharedToUser);
            }
        }

        $this->manager->flush();
    }
}