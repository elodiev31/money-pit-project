<?php

namespace App\Form\ShoppingListProduct;

use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class ShareType extends AbstractType
{
    private ?int $currentUserId;
    public function __construct(private TranslatorInterface $translator)
    {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this->currentUserId = $options['currentUserId'];


        $builder
            ->add('users', EntityType::class, [
                'class' => User::class,
                'choice_label' => 'username',
                'placeholder' => $this->translator->trans('form.placeholder.choose_user'),
                'choice_filter' => ChoiceList::filter($this, function (User $user) {
                    return $user->getId() !== $this->currentUserId;
                }, 'users'),
                'data' => $options['users'],
                'multiple' => true,
                'required' => false,
                'empty_data' => '',
                'label' => $this->translator->trans('form.label.shared_to'),
                'attr' => [
                    'class' => 'form-control form-select',
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => $this->translator->trans('form.button.submit'),
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => null,
            'currentUserId' => null,
            'users' => null
        ]);
    }

}