<?php

namespace App\Form\ShoppingListProduct;

use App\Entity\ShoppingListProduct;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

class AddUpdateType extends AbstractType
{

    public function __construct(private readonly TranslatorInterface $translator)
    {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('product', TextType::class, [
                'label' => $this->translator->trans('form.label.product', [], 'shopping_list_product'),
                'attr' => [
                    'required',
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.label.product', [], 'shopping_list_product')
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrez un produit',
                    ]),
                ]
            ])
            ->add('quantity', IntegerType::class, [
                'label' => $this->translator->trans('form.label.quantity', [], 'shopping_list_product'),
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.label.quantity', [], 'shopping_list_product')
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => $this->translator->trans('form.button.submit'),
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ShoppingListProduct::class
        ]);
    }

}