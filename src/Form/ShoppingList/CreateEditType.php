<?php

namespace App\Form\ShoppingList;

use App\Entity\ShoppingList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

class CreateEditType extends AbstractType
{

    public function __construct(private readonly TranslatorInterface $translator)
    {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => $this->translator->trans('form.label.name', [], 'shopping_list'),
                'attr' => [
                    'required',
                    'class' => 'form-control',
                    'placeholder' => $this->translator->trans('form.label.name', [], 'shopping_list')
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrez un nom pour la liste',
                    ]),
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => $this->translator->trans('form.button.submit', [], 'messages'),
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ShoppingList::class,
        ]);
    }

}