$(function() {
    let $body = $('body');

    // Display modal
    $body.on('click', '.open-modal', function (e) {
        e.preventDefault();

        $.ajax({
            url: $(this).data('url'),
            success: function (data) {
                let mainElement = $(document).find('main')[0];

                mainElement.insertAdjacentHTML('afterend', data);

                let modal = $(document).find('.modal')[0];

                $(modal).addClass('show');
                $(modal).attr('style', 'display: block;');
            }
        });

    });

    // Close the modal
    $body.on('click', '.close', function (e) {
        let modal = $(document).find('.modal')[0];
        $(modal).hide();
    });

    // Submit form and reload the page
    $body.on('click', '.submit', function (e){
        e.preventDefault();

        let form = $('form')[0];

        $.ajax({
            url: form.action,
            type: 'post',
            dataType: 'application/json',
            data: $(form).serialize(),
            success: function (e) {

                let modal = $(document).find('.modal')[0];
                $(modal).hide();
                window.location.reload();
            },
        })
    });

    $body.on('click', '.delete-button', function (e){
        $.ajax({
            url: $(this).data('url'),
            success: function () {
                window.location.reload();
            }
        });
    });

});