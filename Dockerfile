FROM webdevops/php-apache-dev:8.1

# Timezone setup
RUN rm /etc/localtime
RUN ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime

# Install current LTS of NodeJS
RUN curl -fsSL https://deb.nodesource.com/setup_14.x | bash -

# apt packages
RUN apt-get update && apt-get install -y nodejs default-mysql-client

# Install yarn and setup global yarn's bin
RUN npm install --global yarn
RUN yarn config set prefix /usr/local

# install gulp
RUN yarn global add gulp-cli

# Set the final working directory
WORKDIR /app