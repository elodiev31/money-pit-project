# MONEY PIT PROJECT

Money Pit Project is a web application written in Symfony 5.4 and MySQL 8 and using Docker.

## Run Locally

1. Download or clone the project at https://gitlab.com/elodiev31/money-pit.

2. From the terminal, go into the folder of the project and build the environment :
```bash
  docker-compose up -d
```

3. In another terminal or terminal tab, access to the app container to be able to interact with it :
```bash
docker-compose exec web bash
```

4. Into the app container, install the symfony project and its dependencies : 
```bash
composer install
```

5. Create and build the database :
```bash
php bin/console doctrine:database:create

php bin/console doctrine:migrations:migrate
```

6. Build assets :
```bash
yarn install

yarn build
```

7. You can now access to the Money Pit project app and get ready to spend your money !
You can use it at http://127.0.0.1/

