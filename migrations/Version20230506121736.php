<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230506121736 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE shopping_list (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, created DATETIME NOT NULL, INDEX IDX_3DC1A459A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shopping_list_product (id INT AUTO_INCREMENT NOT NULL, shopping_list_id INT NOT NULL, quantity INT DEFAULT NULL, product VARCHAR(255) NOT NULL, INDEX IDX_DD8A4B6623245BF9 (shopping_list_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE shopping_list_product_user (shopping_list_product_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_52E5DE8734FB8E8F (shopping_list_product_id), INDEX IDX_52E5DE87A76ED395 (user_id), PRIMARY KEY(shopping_list_product_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, username VARCHAR(30) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE shopping_list ADD CONSTRAINT FK_3DC1A459A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE shopping_list_product ADD CONSTRAINT FK_DD8A4B6623245BF9 FOREIGN KEY (shopping_list_id) REFERENCES shopping_list (id)');
        $this->addSql('ALTER TABLE shopping_list_product_user ADD CONSTRAINT FK_52E5DE8734FB8E8F FOREIGN KEY (shopping_list_product_id) REFERENCES shopping_list_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE shopping_list_product_user ADD CONSTRAINT FK_52E5DE87A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE shopping_list DROP FOREIGN KEY FK_3DC1A459A76ED395');
        $this->addSql('ALTER TABLE shopping_list_product DROP FOREIGN KEY FK_DD8A4B6623245BF9');
        $this->addSql('ALTER TABLE shopping_list_product_user DROP FOREIGN KEY FK_52E5DE8734FB8E8F');
        $this->addSql('ALTER TABLE shopping_list_product_user DROP FOREIGN KEY FK_52E5DE87A76ED395');
        $this->addSql('DROP TABLE shopping_list');
        $this->addSql('DROP TABLE shopping_list_product');
        $this->addSql('DROP TABLE shopping_list_product_user');
        $this->addSql('DROP TABLE user');
    }
}
